﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using stranger.Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Attack : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    public GameObject rin;
    private Image niaDialogueImage;
    private Image rinDialogueImage;
    public GameObject[] enemies;
    public Turn turn;
    private float targetTime = 0.0f;
    private int counter = 0;
    private bool encounterInitiated = false;
    private bool waitingForInput = false;
    private bool attackedLastRound = true;
    private Health health;
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        health = player.GetComponent<Health>();
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        rin = GameObject.FindWithTag("Rin");
        niaDialogueImage = player.GetComponent<PlayerControl>().dialogueBox.GetComponent<Image>();
        rinDialogueImage = rin.GetComponent<RinNav>().dialogueBox.GetComponent<Image>();
        showDialogueBox(niaDialogueImage, false);
    }

    // Update is called once per frame
    void Update()
    {
        if (enemies != null && player != null)
        {
            foreach (GameObject enemy in enemies)
            {
                if (enemy)
                {
                    float distance = Vector3.Distance(player.transform.position, enemy.transform.position);
                    if (distance < 5)
                    {
                        if (!encounterInitiated)
                        {
                            encounterInitiated = true;
                            InitiateEncounter(enemy);
                        }

                        targetTime -= Time.deltaTime;

                        if (targetTime <= 0.0f)
                        {
                            clearAllDialogueBoxes(enemy);
                            targetTime = 4.0f;
                            triggerNextAttack(counter, enemy);
                            counter++;
                        }
                        int enemyType = enemy.GetComponent<Wander>().monsterType;
                        NiaAttack(enemyType, enemy);
                    }
                }
            }
        }
    }

    //Stop player from moving and turn enemy and character to face each other
    void InitiateEncounter(GameObject enemy)
    {
        if(enemy.GetComponent<Wander>().monsterType == 1)
        {
            // GameObject.FindWithTag("GameController").GetComponent<PauseGame>().Pause();
            SceneManager.LoadScene(4, LoadSceneMode.Additive);
        }
        if(enemy.GetComponent<Wander>().monsterType == 3)
        {
            // GameObject.FindWithTag("GameController").GetComponent<PauseGame>().Pause();
            SceneManager.LoadScene(6, LoadSceneMode.Additive);
        }
        PlayerControl playerMovement = player.GetComponent<PlayerControl>();
        Wander enemyMovement = enemy.GetComponent<Wander>();
        player.GetComponent<Animator>().SetBool("running", false);
        enemy.GetComponent<Animator>().SetBool("walking", false);
        playerMovement.canMove = false;
        enemyMovement.canMove = false;
        player.transform.LookAt(enemy.transform);
        enemy.transform.LookAt(player.transform);

        //set Turn to whoever goes first

    }

    void triggerNextAttack(int moveCounter, GameObject enemy)
    {
        int enemyType = enemy.GetComponent<Wander>().monsterType;
        if (player.GetComponent<PlayerControl>().dialogues.getNextResponseIndex(enemyType) == moveCounter)
        {
            waitingForInput = true;
            NiaAttack(enemyType, enemy);
        }
        else if (rin.GetComponent<RinNav>().dialogues.getNextResponseIndex(enemyType) == moveCounter)
        {
            RinAttack(enemyType, enemy);
        }
        else if (enemy.GetComponent<Wander>().getNextResponseIndex() == moveCounter)
        {
            EnemyAttack(enemyType, enemy);
        }

    }

    void RinAttack(int enemyType, GameObject enemy)
    {
        GameObject dialogueBox = GameObject.FindWithTag("RinDialogueBox");
        showDialogueBox(rinDialogueImage, true);
        GameObject textObject = dialogueBox.transform.GetChild(0).gameObject;
        string nextResponse = rin.GetComponent<RinNav>().dialogues.getNextResponse(enemyType);
        textObject.GetComponent<Text>().text = nextResponse;
    }

    void EnemyAttack(int enemyType, GameObject enemy)
    {
        if(!attackedLastRound)
        { 
            health.removeHealth(.326f);
        }
        clearNumberCounter();
        GameObject dialogueBox = enemy.GetComponent<Wander>().dialogueBox;
        Image enemyDialogue = dialogueBox.GetComponent<Image>();
        showDialogueBox(enemyDialogue, true);
        GameObject textObject = dialogueBox.transform.GetChild(0).gameObject;
        textObject.GetComponent<Text>().text = enemy.GetComponent<Wander>().getNextResponse();
        enemy.GetComponent<Animator>().SetTrigger("attack");
        if(enemy.GetComponent<Wander>().monsterType == 3 && counter == 4)
        {
            SceneManager.LoadScene(13, LoadSceneMode.Additive);
            enemy.GetComponent<Animator>().SetTrigger("die");
            killMonster(enemy);
            endAttackSequence(enemy);
        }
    }

    void clearNumberCounter()
    {
        GameObject dialogueBox = GameObject.FindWithTag("NiaDialogueBox");
        GameObject textObject = dialogueBox.transform.GetChild(1).gameObject;
        textObject.GetComponent<Text>().text = "";
    }

    void NiaAttack(int enemyType, GameObject enemy)
    {
        if (waitingForInput)
        {
            bool niaAttacked = checkForAttack();
            GameObject dialogueBox = GameObject.FindWithTag("NiaDialogueBox");
            showDialogueBox(niaDialogueImage, true);
            if (niaAttacked)
            {
                attackedLastRound = true;
                waitingForInput = false;
                clearNumberCounter();
                GameObject textObject = dialogueBox.transform.GetChild(0).gameObject;
                AttackResponse nextResponse = player.GetComponent<PlayerControl>().dialogues.getNextResponse(enemyType);
                textObject.GetComponent<Text>().text = nextResponse.response;
                player.GetComponent<Animator>().SetBool("punching", true);
                if (nextResponse.isLast)
                {
                    enemy.GetComponent<Animator>().SetTrigger("die");
                    killMonster(enemy);
                    endAttackSequence(enemy);
                }
                else
                {
                    enemy.GetComponent<Animator>().SetTrigger("hurt");
                }
            }
            else
            {
                GameObject textObject = dialogueBox.transform.GetChild(1).gameObject;
                textObject.GetComponent<Text>().text = ((int)targetTime).ToString();
            }
        }
    }

    bool checkForAttack()
    {
        if (Input.GetKey("left"))
        {
            return true;
        }
        return false;
    }

    void showDialogueBox(Image dialogueImage, bool shouldShow)
    {
        if (dialogueImage)
        {
            dialogueImage.enabled = shouldShow;
        }
    }

    void clearAllDialogueBoxes(GameObject enemy)
    {
        //clear the dialogue boxes
        showDialogueBox(niaDialogueImage, false);
        showDialogueBox(rinDialogueImage, false);
        GameObject dialogueBox = enemy.GetComponent<Wander>().dialogueBox;
        Image enemyDialogue = dialogueBox.GetComponent<Image>();
        showDialogueBox(enemyDialogue, false);

        //clear all text
        GameObject textObject = dialogueBox.transform.GetChild(0).gameObject;
        textObject.GetComponent<Text>().text = "";
        textObject = player.GetComponent<PlayerControl>().dialogueBox.transform.GetChild(0).gameObject;
        textObject.GetComponent<Text>().text = "";
        textObject = rin.GetComponent<RinNav>().dialogueBox.transform.GetChild(0).gameObject;
        textObject.GetComponent<Text>().text = "";
    }

    void endAttackSequence(GameObject enemy)
    {
        PlayerControl playerMovement = player.GetComponent<PlayerControl>();
        playerMovement.canMove = true;
    }

    async void killMonster(GameObject enemy)
    {
        if(enemy.GetComponent<Wander>().monsterType == 1)
        {
            SceneManager.LoadScene(5, LoadSceneMode.Additive);
        }
        await Task.Delay(TimeSpan.FromSeconds(5));
        Destroy(enemy);
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        clearAllDialogueBoxes(enemy);
        encounterInitiated = false;
        targetTime = 0.0f;
        counter = 0;
        player.GetComponent<PlayerControl>().dialogues.resetCurrentResponse();
        rin.GetComponent<RinNav>().dialogues.resetCurrentResponse();
    }
}
