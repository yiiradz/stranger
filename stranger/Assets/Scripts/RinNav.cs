﻿using System.Collections;
using System.Collections.Generic;
using stranger.Assets.Scripts;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class RinNav : MonoBehaviour
{
    private NavMeshAgent navAgent;
    public GameObject dialogueBox;

    public RinResponses dialogues;
    // Start is called before the first frame update
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.enabled = true;
        Physics.IgnoreCollision(GameObject.FindWithTag("Player").GetComponent<Collider>(), GetComponent<Collider>());
        if (dialogueBox)
        {
            dialogueBox.GetComponent<Image>().enabled = false;
        }
        dialogues = new RinResponses();
    }

    // Update is called once per frame
    void Update()
    {
        Animate();
        SetNavMesh();
    }

    void SetNavMesh()
    {
        GameObject destination = GameObject.FindWithTag("follow");
        GameObject nia = GameObject.FindWithTag("Player");
        if (destination)
        {
            if (navAgent != null)
            {
                float distance = Vector3.Distance(this.gameObject.transform.position, nia.transform.position);
                if (distance > 3)
                {
                    navAgent.SetDestination(destination.transform.position);
                }
                else
                {
                    navAgent.ResetPath();
                }
            }
            else
            {
                navAgent = GetComponent<NavMeshAgent>();
            }
        }
    }

    void Animate()
    {
        if (this.GetComponent<NavMeshAgent>().velocity.sqrMagnitude > .0000000000000001)
        {
            this.GetComponent<Animator>().SetBool("running", true);
        }
        else
        {
            this.GetComponent<Animator>().SetBool("running", false);
        }
    }
}
