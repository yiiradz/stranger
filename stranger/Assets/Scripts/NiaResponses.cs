namespace stranger.Assets.Scripts
{
    public class NiaResponses
    {
        public AttackResponse[] NaggingFear;
        public AttackResponse[] MonstrousFear;
        public AttackResponse[] NaggingInsecurity;
        public AttackResponse[] OminousInsecurity;

        public int currentResponse;

        public NiaResponses()
        {
            currentResponse = 0;
            NaggingFear = new AttackResponse[4]
            {
                new AttackResponse
                {
                    response = "oh no! It’s Nagging Fears!",
                    sequence = 0,
                    monsterBattle = MonsterType.NaggingFear,
                    isLast = false,
                },
                new AttackResponse
                {
                    response = "It’s okay to be afraid\nat first. But\nI won’t stay afraid",
                    sequence = 3,
                    monsterBattle = MonsterType.NaggingFear,
                    isLast = false,
                },
                new AttackResponse
                {
                    response = "Rin will always be with me",
                    sequence = 5,
                    monsterBattle = MonsterType.NaggingFear,
                    isLast = false,
                },
                new AttackResponse
                {
                    response = "I WILL move forward",
                    sequence = 6,
                    monsterBattle = MonsterType.NaggingFear,
                    isLast = true,
                }
                };

            MonstrousFear = new AttackResponse[3]
            {
                new AttackResponse
                {
                   response = "It might take a while,\nbut I’ll stick with it",
                   sequence = 2,
                   monsterBattle = MonsterType.NaggingFear,
                   isLast = false,
                },
                new AttackResponse
                {
                   response =  "I will make new friends\nand grow even stronger",
                   sequence = 4,
                   monsterBattle = MonsterType.NaggingFear,
                   isLast = false,
                },
                new AttackResponse
                {
                   response = "You cannot stop me",
                   sequence = 6,
                   monsterBattle = MonsterType.NaggingFear,
                   isLast = true
                }
            };
            NaggingInsecurity = new AttackResponse[3] 
            { 
                new AttackResponse
                {
                   response = "I will make new friends!",
                   sequence = 1,
                   monsterBattle = MonsterType.NaggingInsecurity,
                   isLast = false,
                },
                new AttackResponse
                {
                   response = "I will keep learning.",
                   sequence = 3,
                   monsterBattle = MonsterType.NaggingInsecurity,
                   isLast = false,
                },
                new AttackResponse
                {
                   response = "My best is different\nthan everyone else’s.",
                   sequence = 5,
                   monsterBattle = MonsterType.NaggingInsecurity,
                   isLast = true,
                },
            };
            OminousInsecurity = new AttackResponse[] 
            { 

            };
        }

        public AttackResponse getNextResponse(int monster)
        {
            if (monster == 0)
            {
                return NaggingFear[currentResponse++];
            }
            if (monster == 1)
            {
                return MonstrousFear[currentResponse++];
            }
            if (monster == 2)
            {
                return NaggingInsecurity[currentResponse++];
            }
            if (monster == 3)
            {
                return OminousInsecurity[currentResponse++];
            }
            return null;
        }

        public int getNextResponseIndex(int monster)
        {
            if (monster == 0)
            {
                if (NaggingFear.Length >= currentResponse + 1)
                {
                    return NaggingFear[currentResponse].sequence;
                }
            }
            if (monster == 1)
            {
                if (MonstrousFear.Length >= currentResponse + 1)
                {
                    return MonstrousFear[currentResponse].sequence;
                }
            }
            if (monster == 2)
            {
                if (NaggingInsecurity.Length >= currentResponse + 1)
                {
                    return NaggingInsecurity[currentResponse].sequence;
                }
            }
            if (monster == 3)
            {
                if (OminousInsecurity.Length >= currentResponse + 1)
                {
                    return OminousInsecurity[currentResponse].sequence;
                }
            }
            return -1;
        }

        public void skipNextResponse()
        {
            currentResponse++;
        }

        public void resetCurrentResponse()
        {
            currentResponse = 0;
        }
    }
}
