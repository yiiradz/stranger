namespace stranger.Assets.Scripts
{
    public class OminousInsecurityResponses
    {
        public AttackResponse[] responses;
        public int currentResponse;
        public OminousInsecurityResponses()
        {
            currentResponse = 0;
            responses = new AttackResponse[3]
            {
                new AttackResponse
                {
                    response = "You’ll never match up.\nYou won’t be accepted.",
                    sequence = 2,
                    monsterBattle = MonsterType.OminousInsecurity
                },
                new AttackResponse
                {
                    response = "You’ll never fit in.\nYour efforts are meaningless.",
                    sequence = 3,
                    monsterBattle = MonsterType.OminousInsecurity
                },
                new AttackResponse
                {
                    response = "You’re a disappointment.\nYou won’t amount to anything. ",
                    sequence = 4,
                    monsterBattle = MonsterType.OminousInsecurity
                },
            };
        }
    }
}