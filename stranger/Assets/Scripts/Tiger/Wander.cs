﻿using System.Collections;
using System.Collections.Generic;
using stranger.Assets.Scripts;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Wander : MonoBehaviour
{
    public GameObject dest1;
    public GameObject dest2;
    private Vector3 position1;
    private Vector3 position2;
    private NavMeshAgent navAgent;
    public bool canMove;
    public AttackResponse[] dialogues;
    public GameObject dialogueBox;
    public int monsterType;
    public int currentResponse;
    public bool shouldMove;

    // Start is called before the first frame update
    void Start()
    {
        currentResponse = 0;
        canMove = true;
        if (shouldMove)
        {
            navAgent = GetComponent<NavMeshAgent>();
            navAgent.enabled = true;
            position1 = dest1.transform.position;
            position2 = dest2.transform.position;
            if (position1 != null)
            {
                navAgent.SetDestination(position1);
            }
            gameObject.GetComponent<Animator>().SetBool("walking", true);
        }
        if (dialogueBox)
        {
            dialogueBox.GetComponent<Image>().enabled = false;
        }
        AssignResponses();
    }

    // Update is called once per frame
    void Update()
    {
        if (shouldMove)
        {
            if (position1 != null && position2 != null)
            {
                if (navAgent != null && canMove)
                {
                    float distanceToPosition1 = Vector3.Distance(this.gameObject.transform.position, position1);
                    float distanceToPosition2 = Vector3.Distance(this.gameObject.transform.position, position2);
                    if (distanceToPosition1 < 7)
                    {
                        navAgent.SetDestination(position2);
                    }
                    else if (distanceToPosition2 < 7)
                    {
                        navAgent.SetDestination(position1);
                    }
                }
                else
                {
                    navAgent.isStopped = true;
                    navAgent.ResetPath();
                }

            }
        }
    }

    void AssignResponses()
    {
        if (monsterType == 0)
        {
            dialogues = new NaggingFearResponses().responses;
        }
        if (monsterType == 1)
        {
            dialogues = new MonstrousFearResponses().responses;
        }
        if (monsterType == 2)
        {
            dialogues = new NaggingInsecurityResponses().responses;
        }
        if (monsterType == 3)
        {
            dialogues = new OminousInsecurityResponses().responses;
        }
    }

    public string getNextResponse()
    {
        return dialogues[currentResponse++].response;
    }

    public int getNextResponseIndex()
    {
        if (dialogues.Length >= currentResponse + 1)
        {
            return dialogues[currentResponse].sequence;
        }
        return -1;
    }
}
