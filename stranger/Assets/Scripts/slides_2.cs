﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class slides_2 : MonoBehaviour
{
    public Sprite[] slideshow = new Sprite[1];
    public float changeTime = 10.0f;
    private int currentSlide = 0;
    private float timeSinceLast = 1.0f;

    void Start()
    {
        gameObject.GetComponent<UnityEngine.UI.Image>().sprite = slideshow[currentSlide];
       currentSlide++;
    }

    void Update()
    {
        if (timeSinceLast > changeTime && currentSlide < slideshow.Length)
        {
            gameObject.GetComponent<UnityEngine.UI.Image>().sprite = slideshow[currentSlide];
            timeSinceLast = 0.0f;
            currentSlide++;
        }
        // comment out this section if you don't want the slide show to loop
        // -----------------------
        if (currentSlide == slideshow.Length)
        {
            currentSlide = 0;
        }
        // ------------------------
        timeSinceLast += Time.deltaTime;
    }
	async void closeCutscene()
	{
		await Task.Delay(TimeSpan.FromSeconds(4));
		GameObject[] sceneObjects = GameObject.FindGameObjectsWithTag("cutscene");
		foreach (GameObject thing in sceneObjects)
		{
			Destroy(thing);
		}
	}
}
