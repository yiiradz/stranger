namespace stranger.Assets.Scripts
{
    public class NaggingFearResponses
    {
        public AttackResponse[] responses;
        public int currentResponse;
        public NaggingFearResponses()
        {
            currentResponse = 0;
            responses = new AttackResponse[3]
            {
                new AttackResponse
                {
                    response = "You’ll never find a home",
                    sequence = 1,
                    monsterBattle = MonsterType.NaggingFear
                },
                new AttackResponse
                {
                    response = "You’ll be lost forever",
                    sequence = 2,
                    monsterBattle = MonsterType.NaggingFear
                },
                new AttackResponse
                {
                    response = "No one will be\naround to help you",
                    sequence = 4,
                    monsterBattle = MonsterType.NaggingFear
                },
            };
        }
    }
}
