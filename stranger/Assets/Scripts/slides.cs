﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class slides : MonoBehaviour
{
    public Sprite[] slideshow = new Sprite[1];
    public float changeTime = 10.0f;
    private int currentSlide = 0;
    private float timeSinceLast = 1.0f;

    void Start()
    {
       gameObject.GetComponent<UnityEngine.UI.Image>().sprite = slideshow[currentSlide];
       currentSlide++;
    }

    void Update()
    {
        if (timeSinceLast > changeTime && currentSlide < slideshow.Length)
        {
            gameObject.GetComponent<UnityEngine.UI.Image>().sprite = slideshow[currentSlide];
            timeSinceLast = 0.0f;
            currentSlide++;
        }
        if(currentSlide == slideshow.Length)
        {
            closeCutscene();
        }
        timeSinceLast += Time.deltaTime;
    }
    async void closeCutscene()
    {
        await Task.Delay(TimeSpan.FromSeconds(4));
        GameObject[] sceneObjects = GameObject.FindGameObjectsWithTag("cutscene");
        foreach(GameObject thing in sceneObjects)
        {
            Destroy(thing);
        }
    }
}
