﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public Image Bar;
    public float health;

    // Start is called before the first frame update
    void Start()
    {
        health = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        Bar.fillAmount = health;
        checkForNiaDeath();
    }

    public void removeHealth(float healthToRemove)
    {
        health -= healthToRemove;
    }

    void checkForNiaDeath()
    {
        if(health < .035f)
        {
            SceneManager.LoadScene(10);
        }
    }
}
