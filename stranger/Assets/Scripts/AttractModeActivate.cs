﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class AttractModeActivate : MonoBehaviour
{
    public Image videoRenderPlane;

    private float timeOut = 10.0f;
    private float timeOutTimer = 0.0f;

    private void Start()
    {
       videoRenderPlane.GetComponent<Image>().enabled = false;
    }

    private void Update()
    {
        timeOutTimer += Time.deltaTime;
        //if screen tapped, reset timer
        if(Input.GetMouseButtonDown(0))
        {
            timeOutTimer = 0.0f;
            videoRenderPlane.GetComponent<Image>().enabled = false;
        }

        if (timeOutTimer > timeOut)
        {
            videoRenderPlane.GetComponent<Image>().enabled = true;
        }
    }
}
