namespace stranger.Assets.Scripts
{
    public class AttackResponse
    {
        public MonsterType monsterBattle;
        public int sequence;
        public string response;
        public bool isLast;
    }
}