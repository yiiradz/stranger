﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public Transform mainMenu, controlsMenu;

    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
        controlsMenu.gameObject.SetActive(false);
        mainMenu.gameObject.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ToggleControlsMenu(bool toggle)
    {
        if (toggle == true)
        {
            controlsMenu.gameObject.SetActive(toggle);
            mainMenu.gameObject.SetActive(false);
        } else
        {
            controlsMenu.gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }
    }
}
