﻿using System.Collections;
using System.Collections.Generic;
using stranger.Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour
{
    public bool canMove;
    public GameObject dialogueBox;
    public NiaResponses dialogues;
    public int velocity = 0;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        dialogues = new NiaResponses();
        SceneManager.LoadScene(2, LoadSceneMode.Additive);
    }

    // Update is called once per frame
    void Update()
    {
        checkMovement();
        checkPunch();
    }

    void checkPunch()
    {
        if (Input.GetKey("left"))
        {
            this.GetComponent<Animator>().SetBool("punching", true);
        }
        else
        {
            this.GetComponent<Animator>().SetBool("punching", false);
        }
    }

    void checkMovement()
    {
        if (canMove)
        {
            velocity = 25;
            if (Input.GetKey(KeyCode.W))
            {
                this.GetComponent<Animator>().SetBool("running", true);
                if (this.GetComponent<Rigidbody>().velocity.sqrMagnitude < velocity)
                {
                    this.GetComponent<Rigidbody>().AddForce(Vector3.forward * 20);
                }
                transform.rotation = Quaternion.LookRotation(Vector3.forward);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                this.GetComponent<Animator>().SetBool("running", true);
                if (this.GetComponent<Rigidbody>().velocity.sqrMagnitude < velocity)
                {
                    this.GetComponent<Rigidbody>().AddForce(Vector3.right * -20);
                }
                transform.rotation = Quaternion.LookRotation(Vector3.right * -1);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                this.GetComponent<Animator>().SetBool("running", true);
                if (this.GetComponent<Rigidbody>().velocity.sqrMagnitude < velocity)
                {
                    this.GetComponent<Rigidbody>().AddForce(Vector3.forward * -20);
                }
                transform.rotation = Quaternion.LookRotation(Vector3.forward * -1);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.GetComponent<Animator>().SetBool("running", true);
                if (this.GetComponent<Rigidbody>().velocity.sqrMagnitude < velocity)
                {
                    this.GetComponent<Rigidbody>().AddForce(Vector3.right * 20);
                }
                transform.rotation = Quaternion.LookRotation(Vector3.right);
            }
            else
            {
                this.GetComponent<Animator>().SetBool("running", false);
            }
        }
    }
}
