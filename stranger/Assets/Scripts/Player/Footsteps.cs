﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{

    Animator playerAnimator;
    AudioSource audioClip;
    ParticleSystem dogParticles;

    // Start is called before the first frame update
    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        audioClip = GetComponent<AudioSource>();
        dogParticles = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        var emission = dogParticles.emission;
        if (playerAnimator.GetBool("running") && audioClip.isPlaying == false)
        {
            audioClip.volume = Random.Range(0.5f, 0.7f);
            audioClip.pitch = Random.Range(0.8f, 1.1f);
            audioClip.Play();
            emission.rateOverTime = 100;
        } else
        {
            emission.rateOverTime = 0;
        }
    }
}