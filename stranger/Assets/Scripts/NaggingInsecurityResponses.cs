namespace stranger.Assets.Scripts
{
    public class NaggingInsecurityResponses
    {
        public AttackResponse[] responses;
        public int currentResponse;

        public NaggingInsecurityResponses()
        {
            currentResponse = 0;
            responses = new AttackResponse[3]
            {
                new AttackResponse
                {
                    response = "What if I’m always alone?",
                    sequence = 0,
                    monsterBattle = MonsterType.NaggingInsecurity
                },
                new AttackResponse
                {
                    response = "What if I’m rejected?",
                    sequence = 2,
                    monsterBattle = MonsterType.NaggingInsecurity
                },
                new AttackResponse
                {
                    response = "I’ll never be good enough",
                    sequence = 4,
                    monsterBattle = MonsterType.NaggingInsecurity
                },
            };
        }
    }
}