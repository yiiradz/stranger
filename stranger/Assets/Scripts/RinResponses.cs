namespace stranger.Assets.Scripts
{
    public class RinResponses
    {
        public AttackResponse[] MonstrousFear;
        public AttackResponse[] NaggingInsecurity;
        public AttackResponse[] OminousInsecurity;
        public int currentResponse;

        public RinResponses()
        {
            currentResponse = 0;
            MonstrousFear = new AttackResponse[1]
            {
               new AttackResponse
               {
                response = "Be strong, Nia! \nIt’s your deepest fear,\nMonstrous Terror!",
                sequence = 0,
                monsterBattle = MonsterType.OminousInsecurity,
               }
            };
            OminousInsecurity = new AttackResponse[2]
            {
               new AttackResponse
               {
                response = "Be strong, Nia!",
                sequence = 0,
                monsterBattle = MonsterType.OminousInsecurity,
               },
                new AttackResponse
               {
                response = "It’s your deepest Insecurity,\nOminous Insecurity!",
                sequence = 1,
                monsterBattle = MonsterType.OminousInsecurity,
               }
            };
        }

        public string getNextResponse(int monster)
        {
            if (monster == 1)
            {
                return MonstrousFear[currentResponse++].response;
            }
            if (monster == 2)
            {
                return NaggingInsecurity[currentResponse++].response;
            }
            if (monster == 3)
            {
                return OminousInsecurity[currentResponse++].response;
            }
            return "";
        }

        public int getNextResponseIndex(int monster)
        {
            if (monster == 1)
            {
                if (MonstrousFear.Length >= currentResponse + 1)
                {
                    return MonstrousFear[currentResponse].sequence;
                }
            }
            if (monster == 3)
            {
                if (OminousInsecurity.Length >= currentResponse + 1)
                {
                    return OminousInsecurity[currentResponse].sequence;
                }
            }
            return -1;
        }

        public void resetCurrentResponse()
        {
            currentResponse = 0;
        }


    }
}
