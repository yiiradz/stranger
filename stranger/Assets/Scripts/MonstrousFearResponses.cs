namespace stranger.Assets.Scripts
{
    public class MonstrousFearResponses
    {
        public AttackResponse[] responses;
        public int currentResponse;

        public MonstrousFearResponses()
        {
            currentResponse = 0;
            responses = new AttackResponse[3]
            {
                new AttackResponse
                {
                    response = "This will never\nbe your home",
                    sequence = 1,
                    monsterBattle = MonsterType.MonstrousFear
                },
                new AttackResponse
                {
                    response = "You will\nbe alone",
                    sequence = 3,
                    monsterBattle = MonsterType.MonstrousFear
                },
                new AttackResponse
                {
                    response = "The sun will set\nand you will be\nlost forever",
                    sequence = 5,
                    monsterBattle = MonsterType.MonstrousFear
                },
            };
        }
    }
}