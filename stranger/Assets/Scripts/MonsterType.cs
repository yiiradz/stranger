namespace stranger.Assets.Scripts
{
    public enum MonsterType
    {
        NaggingFear = 0,
        MonstrousFear = 1,
        NaggingInsecurity = 2,
        OminousInsecurity = 3
    }
}