﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class finish : MonoBehaviour
{
    bool didPlay = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.FindWithTag("Player");
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if(distance < 8 && !didPlay)
        {
            SceneManager.LoadScene(10, LoadSceneMode.Additive);
            didPlay = true;
            LoadEndScene();
        }
    }

    async void LoadEndScene()
    {
        await Task.Delay(TimeSpan.FromSeconds(5));
        SceneManager.LoadScene(11);
    }
}
