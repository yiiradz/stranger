﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class outro : MonoBehaviour

{
    float timer = 15.0f;
    float ticking;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ticking += Time.deltaTime;
        if (ticking > timer)
        {
            SceneManager.LoadScene(11);
        }

    }
}
